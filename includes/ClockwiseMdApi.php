<?php

/**
 * Class ClockwiseMdApi.
 *
 * @package Drupal\clockwise_md
 */
class ClockwiseMdApi {

  /**
   * The Clockwise.MD API authorization token.
   *
   * @var string
   */
  protected $token;

  /**
   * The Clockwise.MD API api version.
   *
   * @var string
   */
  protected $apiVer;

  /**
   * ClockwiseMdApi constructor.
   */
  public function __construct() {
    $this->token = variable_get('clockwise_md_authorization_token', '');
    $this->apiVer = variable_get('clockwise_md_api_version', '');
  }

  /**
   * Calls the Clockwise.MD API with parameters.
   *
   * @param string $endpoint
   *   The endpoint url without the base api url.
   * @param array $params
   *   Parameters to pass in the url.
   * @param array $add_options
   *   Additional options to pass into the API call.
   * @param string $method
   *   The type of request (ie GET, POST, etc).
   *
   * @return mixed
   *   The API response.
   */
  public function callApi($endpoint, array $params = [], array $add_options = [], $method = 'GET') {

    // Add in out default options to call the API.
    $options = array(
      'method' => $method,
      'timeout' => 20,
      'headers' => array(
        'Content-Type' => 'application/json',
        'Accept' => 'application/json',
        'Authtoken' => $this->token,
      ),
    );

    // Merge in any additional options.
    $options = array_merge($options, $add_options);

    // Setup the URL to call.
    $param = !empty($params) ? '?' . http_build_query($params) : '';
    $url = CLOCKWISE_MD_API_URL . $this->apiVer . $endpoint . $param;

    // Call and return the response.
    $response = drupal_http_request($url, $options);
    return json_decode($response->data, TRUE);
  }

}
