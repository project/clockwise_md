<?php

/**
 * @file
 * Admin File for Clockwise.MD API.
 */

/**
 * Clockwise.MD API Settings Form.
 */
function clockwise_md_settings_form($form, &$form_state) {

  // Dev fieldset.
  $form['dev'] = array(
    '#type' => 'fieldset',
    '#title' => t('Clockwise.MD API Settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  // The API Version.
  $odesc = t('This should not change. This is here for future proofing.');
  $form['dev']['clockwise_md_api_version'] = array(
    '#type' => 'textfield',
    '#title' => t('Clockwise.MD API Version'),
    '#default_value' => variable_get('clockwise_md_api_version', 'v1'),
    '#description' => $odesc,
    '#required' => TRUE,
  );

  // The Access Key.
  $tdesc = t('You can grab your access key from your <a href="@app" target="_blank">Clockwise.MD Docs Page</a>.', array('@app' => 'https://www.clockwisemd.com/docs'));
  $form['dev']['clockwise_md_authorization_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Clockwise.MD Access Key'),
    '#default_value' => variable_get('clockwise_md_authorization_token', ''),
    '#description' => $tdesc,
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
