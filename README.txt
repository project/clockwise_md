CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration


INTRODUCTION
------------

This Clockwise.MD API module is for connecting and calling their API. It is
a simple module, but it gives you a method and a class to work with to call
the API to get results.

 * For a full description on how to use the API portion of the module, visit
   the project page: https://drupal.org/project/clockwise_md


REQUIREMENTS
------------

This module has no dependencies.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------

 * Configure user permissions in Administration » People » Permissions:

    - Administer Clockwise MD API Settings

     Allows users to see and use the Clockwise.MD config setting page
     located at: /admin/config/services/clockwise

 * Add in your Clockwise.MD API Authorization Key in Administration »
   Configuration » Clockwise.MD API Settings.
